var pgp = require('pg-promise')();
var db = pgp("postgres://<connection-string>/fakesu");
var bcrypt = require('bcryptjs');
var shortid = require('shortid');
var uuid = require('uuid/v4');
var captchaGenerator = require('trek-captcha');

var redis = require("redis"),
    client =  require('bluebird').promisifyAll(redis.createClient());
	
module.exports.setup = function(app) {
	/**
	* @swagger
	* definitions:
	*   Target:
	*     required:
	*       - id
	*       - fake
	*       - dang
	*       - votes
	*     properties:
	*       id:
	*         type: string
	*       fake:
	*         type: integer
	*       dang:
	*         type: integer
	*       votes:
	*         type: integer
	*/
   
	/**
	* @swagger
	* /register:
	*   post:
	*     description: Регистрация
	*     parameters:
	*     - name: name
	*       description: Имя
	*       in: formData
	*       required: true
	*       type: string
	*     - name: login
	*       description: Логин
	*       in: formData
	*       required: true
	*       type: string
	*     - name: email
	*       description: Почта
	*       in: formData
	*       required: true
	*       type: string
	*     - name: identifier
	*       description: Идентификатор сессии
	*       in: formData
	*       required: true
	*       type: string
	*     - name: token
	*       description: Токен безопасности
	*       in: formData
	*       required: true
	*       type: string
	*     - name: password
	*       description: Пароль
	*       in: formData
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Успешная регистрация		 
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/register", async function(req, res) {
		try {
			var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

			var identifier = req.body.identifier;
			var token = req.body.token;
		
			var trueToken = await client.getAsync(`register:${ip}:${identifier}`);
			if(trueToken == null || token != trueToken)
				return res.send(400, "Неверный токен безопасности!");				
			await client.delAsync(`register:${ip}:${identifier}`);
			
			var raw_password = req.body.password;
			var name = req.body.name;
			var email = req.body.email;
			var login = req.body.login;
			var token = req.body.token;
			
			var password = bcrypt.hashSync(raw_password, 10);
			
			var code = shortid.generate();
			
			db.query('INSERT INTO users(name, email, login, password, code) VALUES(${name}, ${email}, ${login}, ${password}, ${code})', {
				name, email, login, password, code
			}).then(data => {
				return res.json("true");
			}).catch(error => {
				return res.json(400, "false");
			});	
		} catch(ex) {
			res.send(400);
		}
	});
	
	/**
	* @swagger
	* /register/captcha:
	*   get:
	*     description: Запрос идентификатора и капчи для регистрации 
	*     responses:
	*       200:
	*         description: Получена base64 капча и идентификатор	 
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.get("/register/captcha", async function(req, res) {
		try {
			var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		
			var keys = await client.scanAsync('0', 'MATCH', `register:${ip}:*`, 'COUNT', '20');
			if(keys[1].length >= 10) 
				return res.send(423, "Превышен лимит запросов для IP адреса");
			
			var identifier = shortid.generate();
			var { token, buffer } = await captchaGenerator();
			await client.setAsync(`register:${ip}:${identifier}`, token, 'EX', 240);
			var captcha = buffer.toString('base64');
			
			res.json({ identifier, captcha });
		} catch(ex) {
			res.send(400, ex);
		}
	});
	
	/**
	* @swagger
	* /login:
	*   post:
	*     description: Авторизация
	*     parameters:
	*     - name: login
	*       description: Логин
	*       in: formData
	*       required: true
	*       type: string
	*     - name: password
	*       description: Пароль
	*       in: formData
	*       required: true
	*       type: string
	*       format: password
	*     responses:
	*       200:
	*         description: Успешная авторизация	(возвращает строковую сессию)
	*       404:
	*         description: Пользователь не найден
	*       401:
	*         description: Неверный пароль
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/login", function(req, res) {
		try {
			var login = req.body.login;
			var password = req.body.password;
			
			db.query('SELECT * FROM users WHERE login = ${login} LIMIT 1', {
				login,
			}).then(async data => {
				if(data.length == 1) {
					if(!bcrypt.compareSync(password, data[0].password))
						return res.send(401, "Неверный пароль");
					var key = uuid();
					await client.setAsync(key, data[0].id);
					//client.expire(key, 500);
					res.json(key);
				}
				else
					res.send(404, "Пользователь не найден");
			}).catch(error => {
				res.json(400, error);
			});
		} catch(ex) {
			res.send(400, ex);
		}
	});

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Получить данные о целе
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.get("/target/:id", function(req, res) {
		try {
			var target_id = req.params.id;
			db.query('SELECT id, fake, dang, votes FROM targets WHERE id = ${target_id} LIMIT 1', {
				target_id,
			}).then(data => {
				if(data.length == 1)
					res.json(data[0]);
				else
					res.send(404);
			}).catch(error => {
				res.json(400, error);
			});
		} catch(ex) {
			res.send(400, ex);
		}
	});

	/**
	* @swagger
	* /chunk/{id}:
	*   get:
	*     description: Получить чанк целей (подписанный)
	*     parameters:
	*     - name: id
	*       description: Идентификатор чанка
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Чанк найден
	*       404:
    *         description: Чанк не найден
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.get("/chunk/:id", function(req, res) {
		try {
			var chunk_id = req.params.id;
			db.query('SELECT id, fake, dang, votes FROM targets WHERE chunk = ${chunk_id}', {
				chunk_id,
			}).then(data => {
				if(data.length == 1)
					res.json(data[0]);
				else
					res.send(404);
			}).catch(error => {
				res.json(400, error);
			});
		} catch(ex) {
			res.send(400, ex);
		}
	});
	
	/**
	* @swagger
	* /vote/{id}:
	*   post:
	*     description: Проголосовать за цель
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     - name: session
	*       description: Сессия
	*       in: formData
	*       required: true
	*       type: string
	*     - name: fake
	*       description: Тип фейка
	*       in: formData
	*       required: true
	*       type: integer
	*       format: int32
	*     - name: dang
	*       description: Уровень опасности
	*       in: formData
	*       required: true
	*       type: integer
	*       format: int32
	*     responses:
	*       200:
	*         description: Голос засчитан
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/vote/:id", async function(req, res) {
		try {
			var dang = req.body.dang;
			var fake = req.body.fake;
			//var spec = req.body.spec;
			var comment = req.body.comment || null;
			var session = req.body.session;
			
			var user_id = await client.getAsync(session);
			if(user_id == null)
				return res.send(403);
			
			var target_id = req.params.id;
			
			if(target_id == null || target_id.length < 3 || target_id[2] != '_') return res.json("null"); //Должна быть здесь валидация ID
			
			db.query('INSERT INTO votes(user_id, target_id, fake, dang, comment) VALUES(${user_id}, ${target_id}, ${fake}, ${dang}, ${comment})', {
				user_id, target_id, fake, dang, comment
			}).then(data => {
				return res.json("true");
			}).catch(error => {
				return res.json(JSON.stringify(error));
			});	
		} catch(ex) {
			res.send(400, ex);
		}
	});

	/**
	* @swagger
	* /myvote/{id}:
	*   post:
	*     description: Получить результат голосования по цели
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     - name: session
	*       description: Сессия
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Запрос успешно выполнен (возвращает fake, dang)
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/myvote/:id", async function(req, res) {
		try {
			var target_id = req.params.id;
			var session = req.body.session;
			var user_id = await client.getAsync(session);

			if(user_id == null)
				return res.send(403);
			
			db.query('SELECT * FROM votes WHERE user_id = ${user_id} AND target_id = ${target_id} LIMIT 1', {
				user_id, target_id
			}).then(data => {
				if(data.length == 1)
					res.json(data[0]);
				else
					res.send(404);
			}).catch(error => {
				res.json(400, error);
			});
		} catch(ex) {
			res.send(400, ex);
		}
	});
	
	/**
	* @swagger
	* /request/{id}:
	*   post:
	*     description: Запрос на проверку
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Запрос успешно отправлен
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/request/:id", async function(req, res) {
		try {
			var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
			var target_id = req.params.id;
		
			if(target_id == null || target_id.length < 3 || target_id[2] != '_') return res.json("null"); //Должна быть здесь валидация ID
			
			var keys = await client.scanAsync('0', 'MATCH', `request:${ip}:*`, 'COUNT', '20');
			if(keys[1].length >= 10) 
				return res.send(423, "Превышен лимит запросов для IP адреса");
			
			await client.setAsync(`request:${ip}:${target_id}`, '');
			res.json(true);
		} catch(ex) {
			res.send(400);
		}
	});
	
	/**
	* @swagger
	* /arbitrage/{id}:
	*   post:
	*     description: Запрос на арбитражную проверку
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     - name: session
	*       description: Сессия
	*       in: formData
	*       required: true
	*       type: string
	*     responses:
	*       501:
	*         description: Пока не реализовано	
	*/
	app.post("/arbitrage/:id", async function(req, res) {
		res.send(501);
	});
	
	/**
	* @swagger
	* /unvote/{id}:
	*   post:
	*     description: Запрос на аннулирование голоса
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     - name: session
	*       description: Сессия
	*       in: formData
	*       required: true
	*       type: string
	*     responses:
	*       501:
	*         description: Пока не реализовано	
	*/
	app.post("/unvote/:id", function(req, res) {
		res.send(501);
	});
};