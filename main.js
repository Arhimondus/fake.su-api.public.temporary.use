﻿var restify = require('restify');
var plugins = require('restify-plugins');
var cors = require('cors');
var swagger = require('restify-swagger-jsdoc');

var app = restify.createServer({
	name: 'Fake.su api',
	version: '0.0.1',
	//key: selfSigned.key,
	//cert: selfSigned.cert
});

app.use(plugins.acceptParser(app.acceptable));
app.use(plugins.queryParser());
app.use(plugins.bodyParser());
app.use(cors());

var api = require('./api');
api.setup(app);

swagger.createSwaggerPage({
    title: 'API documentation', // Page title (required) 
    version: '1.0.0', // Server version (required) 
    server: app, // Restify server instance created with restify.createServer() 
    path: '/docs/swagger', // Public url where the swagger page will be available 
    apis: [ `${__dirname}/api.js` ], // Path to the API docs 
    //routePrefix: null // prefix to add for all routes (optional) 
});

app.listen(80, function () { //3030
	console.log('%s listening at %s', app.name, app.url);
});